ARG NODE_VERSION=10-alpine
FROM node:$NODE_VERSION
ARG QUASAR_VERSION=0.15.16

RUN yarn global add quasar-cli@$QUASAR_VERSION

RUN yarn cache clean
WORKDIR /node/app
USER node