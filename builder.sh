#!/bin/bash

get_latest_release() {
  curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}

LATEST=$(get_latest_release "quasarframework/quasar")


for node_version in "8-apine" "9-alpine" "10-alpine"
do 
    export NODE_VERSION=$node_version
    for quasar_version in "0.15.16" "0.16.0" "0.17.0" $LATEST
    do 
        export QUASAR_VERSION=$quasar_version
        docker build -t $REGISTRY/ziul/$IMAGE:$NODE_VERSION-$QUASAR_VERSION --build-arg QUASAR_VERSION=$QUASAR_VERSION --build-arg NODE_VERSION=$NODE_VERSION .
        docker push $REGISTRY/ziul/$IMAGE:$NODE_VERSION-$QUASAR_VERSION
    done
done
